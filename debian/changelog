gitinspector (0.4.4+dfsg-15) unstable; urgency=medium

  * Team upload.
  * Drop dependency on python3-pkg-resources (Closes: #1083417)
  * Fix SyntaxWarning (Closes: #1085609)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 05 Feb 2025 19:46:09 +0100

gitinspector (0.4.4+dfsg-14) unstable; urgency=medium

  * Team upload.
  * Tweak python3 patch to drop dependency on python3-unittest2
    (Closes: #1058982)

 -- Alexandre Detiste <tchet@debian.org>  Tue, 02 Jan 2024 11:30:49 +0100

gitinspector (0.4.4+dfsg-13) unstable; urgency=medium

  * d/s/options: Ignore .egg-info in diffs (Closes: #1044298)

 -- Christian Kastner <ckk@debian.org>  Tue, 15 Aug 2023 17:51:17 +0200

gitinspector (0.4.4+dfsg-12) unstable; urgency=medium

  * Fix d/watch
  * py3versions: Use --supported instead of --requested
  * Add lintian override for false positive
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * Bump copyright
  * Extend patch documentation

 -- Christian Kastner <ckk@debian.org>  Sun, 18 Dec 2022 21:22:54 +0100

gitinspector (0.4.4+dfsg-11) unstable; urgency=medium

  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Bump debhelper compatibility level to 13

 -- Christian Kastner <ckk@debian.org>  Tue, 22 Feb 2022 20:54:04 +0100

gitinspector (0.4.4+dfsg-10) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Wed, 15 Sep 2021 00:10:10 -0400

gitinspector (0.4.4+dfsg-9) unstable; urgency=medium

  * autopkgtest: Add allow-stderr restriction

 -- Christian Kastner <ckk@debian.org>  Fri, 17 Apr 2020 14:10:45 +0200

gitinspector (0.4.4+dfsg-8) unstable; urgency=medium

  * Fix bug number in previous changelog entry (914 -> 941)
  * autopkgtest: Add python3-all to Depends

 -- Christian Kastner <ckk@debian.org>  Mon, 06 Apr 2020 10:38:32 +0200

gitinspector (0.4.4+dfsg-7) unstable; urgency=medium

  [ Louis-Philippe Véronneau ]
  * d/control: use the right name for the PAPT.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Christian Kastner ]
  * Add Drop-superfluous-argument-to-NullTranslations.install.patch.
    Fixes instant crash with Python 3.8. (Closes: #955284)
  * d/control: Set Rules-Requires-Root to no
  * Bump Standards-Version to 4.5.0
  * Add superficial autopkgtests
  * Update Use-jQuery-libraries-from-the-Debian-archive.patch.
    (Closes: #941572)
  * Add Drop-bufsize-argument-to-Popen.patch
  * Add Repository{,-Browse} to upstream metadata

 -- Christian Kastner <ckk@debian.org>  Mon, 30 Mar 2020 21:33:48 +0200

gitinspector (0.4.4+dfsg-6) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.

  [ Sandro Tosi ]
  * Team upload.
  * port application to python3 and use it for packaging; Closes: #936616

 -- Sandro Tosi <morph@debian.org>  Thu, 26 Sep 2019 13:25:26 -0400

gitinspector (0.4.4+dfsg-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright:
    - Use https protocol in Format field
  * d/changelog:
    - Remove trailing whitespaces
  * d/control:
    - Remove ancient X-Python-Version field

  [ Stefano Rivera ]
  * Remove debian dir from .gitignore
  * DEP-14 branch scheme
  * Migrate to Git on Salsa

  [ Christian Kastner ]
  * d/control:
    - Bump Standards-Version to 4.3.0 (no changes needed)
    - Switch Build-Depends from debhelper to debhelper-compat
    - Bump Build-Depends for debhelper-compat to 12
  * d/compat:
    - Drop, as made obsolete by debhelper-compat
  * d/copyright:
    - Bump copyrights
    - Add Files-Excluded field to exclude minimized jQuery
  * d/watch:
    - Convert to format version 4
  * d/rules:
    - Drop get-orig-source target
      uscan will pick up Files-Excluded from d/copyright
  * Add more options to gbp.conf

 -- Christian Kastner <ckk@debian.org>  Sun, 10 Feb 2019 21:21:35 +0100

gitinspector (0.4.4+dfsg-4) unstable; urgency=medium

  * d/control:
    - Add python-pkg-resources to Depends

 -- Christian Kastner <ckk@debian.org>  Mon, 25 Jul 2016 22:07:20 +0200

gitinspector (0.4.4+dfsg-3) unstable; urgency=medium

  * debian/patches (updated):
    - Use-jQuery-libraries-from-the-Debian-archive.patch
      Fix typo breaking jquery-tablesorter inclusion. Closes: #831817

 -- Christian Kastner <ckk@debian.org>  Tue, 19 Jul 2016 20:49:10 +0200

gitinspector (0.4.4+dfsg-2) unstable; urgency=medium

  * d/control:
    - Bump Standards-Version to 3.9.8 (no changes needed)

 -- Christian Kastner <ckk@debian.org>  Sun, 17 Apr 2016 19:58:39 +0200

gitinspector (0.4.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * d/control:
    - Bump Standards-Version to 3.9.7 (no changes needed)
    - Switch Vcs-Browser from insecure URI to secure one

 -- Christian Kastner <ckk@debian.org>  Sun, 07 Feb 2016 17:12:42 +0100

gitinspector (0.4.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * d/control:
    - Add python-unittest2 to Build-Depends, fixing a FTBFS. Closes: #804426
  * d/patches (updated):
    - Use-jQuery-libraries-from-the-Debian-archive.patch

 -- Christian Kastner <ckk@debian.org>  Sun, 08 Nov 2015 14:38:26 +0100

gitinspector (0.4.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * d/patches (refreshed):
    - Use-jQuery-libraries-from-the-Debian-archive.patch
    - Move-arch-indep-files-to-usr-share.patch

 -- Christian Kastner <ckk@debian.org>  Fri, 23 Oct 2015 14:13:29 +0200

gitinspector (0.4.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * d/control:
    - Add to Build-Depends:
      + asciidoc
      + libxml2-utils
      + xsltproc
      + docbook-xml
      + docbook-xsl
    - Replace my email in Updaters with my @debian.org address
    - Update Homepage, upstream moved to GitHub
  * d/rules:
    - Build documentation from source
  * d/doc-base, d/docs:
    - Install HTML documentation
  * d/man:
    - Drop manpage, upstream provides one now
  * d/copyright:
    - Update copyright
  * d/watch:
    - Upstream moved to GitHub
  * d/patches (dropped):
    - Print-usage-when-called-without-any-args.patch
    - Add-missing-HTML-footer-to-htmlembedded-output.patch
    - fix-C-locale-crash.patch
      All three included upstream
  * d/patches (refreshed):
    - Use-jQuery-libraries-from-the-Debian-archive.patch
     -Move-arch-indep-files-to-usr-share.patch

 -- Christian Kastner <ckk@debian.org>  Wed, 30 Sep 2015 17:53:50 +0200

gitinspector (0.3.2+dfsg-2) unstable; urgency=medium

  * debian/patches (added):
    - fix-C-locale-crash
      Don't throw an exception when using the C locale. Fixes a FTBFS.
      Closes: #773143

 -- Christian Kastner <debian@kvr.at>  Fri, 19 Dec 2014 21:36:17 +0100

gitinspector (0.3.2+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #768508)

 -- Christian Kastner <debian@kvr.at>  Thu, 06 Nov 2014 12:21:31 +0100
